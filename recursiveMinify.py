import requests
import os

#minify all css files 
#in a folder
class RecursiveMinify:

    def __init__(self):
        self.__files = []

    def get_files(self):
        return self.__files
    
    def set_files(self, files):
        self.__files = files

    def iterateFiles(self, path):
        files = []
        for root, dirs, filenames in os.walk(path):
            for file in filenames:
                if '.js' in file:
                    files.append(os.path.join(root, file))  
        return files
    
    #use this to minify local css files
    def minify_file_with_css_minifier(self, file):
        url = 'https://javascript-minifier.com/raw'
        data = {'input': open(file, 'rb').read()} #https://fonts.googleapis.com/css?family=Arimo:regular,italic,700,700italic&ver=1.2.7%27 
        response = requests.post(url, data=data)
        return response
            
    # creating and writng to a file
    def create_and_write_to_a_file(self, data, file):
        print(file)
        f=open(file, "w")
        f.write('') 
        f.write(data.text) # use the requested data.
        f.close()

def main():
    minifierObject = RecursiveMinify()
    #path to get all the css files from
    path = 'js2'
    files = minifierObject.iterateFiles(path)

    #set files
    minifierObject.set_files(files)

    files = minifierObject.get_files()

    #for each file
    #minfy first
    #then write to the file
    for file in files:
        response = minifierObject.minify_file_with_css_minifier(file)
        minifierObject.create_and_write_to_a_file(response, file)

if __name__ == "__main__":
    main()