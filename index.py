import requests
import os

#use this to minify local css files
def minify_file_with_css_minifier(file):
        url = 'https://javascript-minifier.com/raw'
        data = {'input': open(file, 'rb').read()} #https://fonts.googleapis.com/css?family=Arimo:regular,italic,700,700italic&ver=1.2.7%27 
        print(data)
        response = requests.post(url, data=data)
        #print("minified file content of a local file" + response.text)
        return response

#use this to get the remote css content
def get_contents_of_the_remote_file_with_built_in_request(file):
    response = requests.get(file, timeout=5)
    #print("remote file contents: " + response.text)
    return response

# reading a local file content
def read_content_of_the_local_file():
    with open('test.js', 'r') as f:
        data = f.read()
        print(data)

# creating and writng to a file
def create_and_write_to_a_file(data):
    f=open("temp.js", "a+")
    f.write(''); 
    f.write(data.text) # use the requested data.


def delete_a_file(file):
    os.remove(file)


#file_name = "test.js"
file_name = "https://eraofecom.org/wp-content/plugins/portfolio-wp/js/crp-tiled-layer.js?ver=5.0.4"

if "http" in file_name:
       
        response2 = get_contents_of_the_remote_file_with_built_in_request(file_name)

        f=open("temp2.js", "a+")
        f.write(''); 
        f.write(response2.text)
        f.close()
        response = minify_file_with_css_minifier("temp2.js")
        delete_a_file('temp2.js')
        
else:
        response = minify_file_with_css_minifier(file_name)
        
create_and_write_to_a_file(response)

