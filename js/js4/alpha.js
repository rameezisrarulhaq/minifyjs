

jQuery(document).ready(function(){
    
 jQuery(function(jQuery){
        var $obj ;
    
        jQuery("#banner-section #typed").typed({
            
             //strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
            stringsElement: jQuery('#banner-section #typed-strings'),
            typeSpeed: 90,
            backDelay:0,
            loop: false,
            contentType: 'html', // or text
            // defaults to false for infinite loop
            loopCount: false,
            callback: function(){ 
            foo();
            jQuery('#banner-section .typed-cursor').css('display','none');
          
            jQuery('#banner-section .underline_robots_path').css('display','block');
          
             },
            resetCallback: function() { newTyped(); }
        });

        jQuery("#banner-section .reset").click(function(){
            
            jQuery("#banner-section #typed").typed('reset');
        });
        
    }); 
    
    function newTyped(){ /* A new typed object */ }
    function foo(){ //    console.log("Callback"); 
    }
});
